//
//  main.cpp
//  pipeflow
//
//  Created by Ryohei Seto on 2021/04/04.
//

#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <csignal>
#include <getopt.h>
#include <vector>
#include <fstream>
#include <cmath>

class PhiField {
public:
	/* variables */
	std::vector <double> phi;
	int ny;
	double phi_total;
	double phi_jamming;
	PhiField (int ny_, double value)
	{
		ny = ny_;
		phi.resize(ny);
		for (int i=0; i< ny; i++){
			phi[i] = value;
		}
		phi_total = 0;
		for (auto p : phi) {
			phi_total += p;
		}
	}

	double getValue (int i)
	{
		if (i < ny) {
			return phi[i];
		} else {
			exit(1);
		}
	}

	void increment (double d_phi, int i)
	{
		phi[i] += d_phi;
	}
	
	void set (double value, int i)
	{
		phi[i] = value;
	}

	void normalize() {
		double tmp_total = 0;
		for (int i=0; i<ny; i++) {
			tmp_total += phi[i];
		}
		for (int i=0; i<ny; i++) {
			phi[i] *= phi_total/tmp_total;
		}
	}
	
	void redistribution(double init_profile) {
		for (int i=0; i<ny; i++) {
			phi[i] += init_profile*(i - ny/2);
		}
		normalize();
	}
};

double sedimentationHindranceFunc(double phi, double phij, double alpha)
{
	//return (1-phi/phij)*pow(1-phi, alpha-1);
	return pow(1-phi, alpha);
}


double viscosityFunction(double phi, double phij)
{
//	if (phi > phij) {
//		std::cerr << phi << " > " << phij << std::endl;
//	}
	double j_factor = 1/(1-phi/phij);
	return pow(j_factor, 2);
}


void calcShearRate(std::vector <double> &shearrate,
				   PhiField &phi,
				   double dy, double p_grad )
{
	int ny = phi.ny;
//	for (int j=0; j < j_jam; j++) {
//		shearrate[j] = 0;
//	}
	double phi_j;
	double phi_jm1;
	for (int j=0; j <= ny; j++) {
//		if (j <= j_jam) {
//			shearrate[j] = 0;
//		} else {
		double phi_mid;
		double y = dy*j;
		if (j != ny) {
			phi_j = phi.getValue(j);
			if (j == 0) {
				phi_jm1 = phi_j;
			} else {
				phi_jm1 = phi.getValue(j-1);
			}
			phi_mid = (phi_j + phi_jm1)/2;
		} else if (j == 0) {
			phi_mid = phi.getValue(0);
		} else {
			phi_mid = phi.getValue(ny-1);
		}
		shearrate[j] = p_grad*y /viscosityFunction(phi_mid, phi.phi_jamming);
	}
//	}
}

void calcFlux(std::vector <double> &flux,
			  PhiField &phi,
			  std::vector <double> &pp,
			  double dy,
			  double alpha)
{
	double c = (2.0/9);
	int ny = phi.ny;
	/* if j_jam = -1, flux[0] = 0;
	 * if j_jam >= 0, flux[j_jam+1] = 0;
	 *
	 */
	flux[0] = 0;
	for (int j=1; j < ny; j++) {
		double phi_ = (phi.getValue(j)+phi.getValue(j-1))/2;
		flux[j] = -c*sedimentationHindranceFunc(phi_, phi.phi_jamming, alpha)*(pp[j]-pp[j-1])/dy;
	}
	flux[ny] = 0;
}

double Power(double x , int n)
{
	return pow(x, n);
}

double macroFrictionCoeff(double phi) {
	//return 55.220699668954424 - 417.7250336378371*phi + 1301.0364293884709*Power(phi,2) - \
	2037.9539315048664*Power(phi,3) + 1601.4678555577887*Power(phi,4) - \
	507.1043435202081*Power(phi,5);
	return 0.3/(phi*phi);
}

void calcParticlePressure(std::vector <double> &pp,
						  PhiField &phi,
						  std::vector <double> &shearstress,
						  double dy)
{
	// y = j*dy;
	double elastic_constant = 500;
	int ny = phi.ny;
	double max_p = 0;
	for (int j=0; j < ny; j++) {
		double phi_ = phi.getValue(j);
		pp[j] = shearstress[j]/macroFrictionCoeff(phi_);
		if (phi_ > phi.phi_jamming) {
			double p_elastic = elastic_constant*(phi_ - phi.phi_jamming);
			pp[j] += p_elastic;
			if (max_p < p_elastic) {
				max_p = p_elastic;
			}
		}
	}
//	if (max_p > 0) {
//		std::cerr << ' ' << max_p  << std::endl;
//	}

}

void output (std::ofstream &foutput,
			 std::vector <double> shearrate,
			 PhiField &phi,
			 std::vector <double> &shearstress,
			 std::vector <double> &pp,
			 double dy,
			 double y_jam,
			 double p_grad)
{
	calcShearRate(shearrate, phi, dy, p_grad);
	
	int ny = phi.ny;
	std::vector <double> ux;
	ux.resize(ny+1);
	ux[ny] = 0;
	for (int j=0; j < ny; j++) {
		ux[ny-(j+1)] = ux[ny-j] + shearrate[ny-j]*(-dy);
	}
	for (int j=0; j < ny; j++) {
		double phi_ = phi.getValue(j);
		double shear_rate = 0.5*(shearrate[j]+shearrate[j+1]);
		double y = j*dy + dy/2;
		foutput << y << ' ';
		foutput << phi_ << ' ';
		foutput << shear_rate << ' ';
		foutput << pp[j]<< ' ';
		foutput << shearstress[j] << ' ';
		foutput << ux[j]/ux[0] << ' ';
		foutput << ux[j]  << ' ';
		if (y < y_jam) {
			foutput << phi.phi_jamming << ' ';
		} else {
			foutput << phi.phi_jamming*sqrt(y_jam/y) << ' ';
		}
		foutput << std::endl;
	}
	foutput << std::endl;
}

int main(int argc, const char * argv[]) {
	std::vector <double> shearrate;
	std::vector <double> shearstress;
	std::vector <double> pp;
	std::vector <double> flux;
	std::vector <double> phi_predictor;
	std::vector <double> phi_corrector;
	int ny = 100;
	double p_grad = 1;
	double ly = 1.0;
	double phi_ave = atof(argv[2]);
	double dy = ly/ny;
	
	PhiField phi(ny, phi_ave);
	phi.phi_jamming = atof(argv[1]);
	double initial_profile = 0;
	double alpha = 2;
	std::cerr << " argc = " << argc << std::endl;
	if (argc == 4) {
		//initial_profile = atof(argv[3]);
		alpha = atof	(argv[3]);
		std::cerr << "alpha = " << alpha << std::endl;
	}
	std::cerr << " argc = " << argc << std::endl;
	phi.redistribution(initial_profile);
	std::ofstream fout;
	std::ofstream flog;
	std::stringstream filename;
	std::stringstream logname;
	filename << "data_" << phi.phi_jamming << "_" << phi_ave << "_" << alpha << ".dat";
	logname << "log_" << phi.phi_jamming << "_" << phi_ave << "_" << alpha << ".dat";
	fout.open(filename.str());
	flog.open(logname.str());

	shearrate.resize(ny+1);
	pp.resize(ny);
	flux.resize(ny+1);
	phi_predictor.resize(ny);
	phi_corrector.resize(ny);

	shearstress.resize(ny);
	// initial value
	double dt = 0.1*dy*dy;
	
	std::cerr << "dt = " << dt << std::endl;
	
	int log_interval = 0.05/dt;
	int k = 0;
	double y_jam = pow(1-sqrt(1-phi_ave/phi.phi_jamming),2);
	
	for (int j=0; j < ny; j++) {
		double y = dy*(j+0.5);
		shearstress[j] = p_grad*y;
	}
	while (true) {
		calcParticlePressure(pp, phi, shearstress, dy);
		calcFlux(flux, phi, pp, dy, alpha);
		for (int j=0; j < ny; j++) {
			phi_predictor[j] = (-flux[j+1]+flux[j])/dy;
		}
		for (int j=0; j < ny; j++) {
			phi.increment(phi_predictor[j]*dt, j);
		}
		calcParticlePressure(pp, phi, shearstress, dy);
		calcFlux(flux, phi, pp, dy, alpha);
		for (int j=0; j < ny; j++) {
			phi_corrector[j] = (-flux[j+1]+flux[j])/dy;
			phi.increment(0.5*(phi_corrector[j]-phi_predictor[j])*dt, j);
		}
		phi.normalize();

		if (k++ % log_interval == 0) {
			output (flog, shearrate, phi, shearstress, pp, dy, y_jam, p_grad);
			double diff_pressure = abs(pp[0] - pp[ny-1]);
			std::cerr << diff_pressure << std:: endl;
			if ( diff_pressure < 1e-6 ) {
				break;
			}
		}
	}
	output (fout, shearrate, phi, shearstress, pp, dy, y_jam, p_grad);
	
	flog.close();
	fout.close();
	return 0;
}
